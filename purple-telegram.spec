Name:		purple-telegram
Version:	1.2.5
Release:	2%{?dist}
Summary:	Libpurple protocol plugin for Telegram support
Group:		Applications/Internet
License:	GPLv2+ and LGPLv2+
URL:		https://github.com/majn/telegram-purple
# Source archive made by following commands:
# git clone --recursive --branch v1.2.5 https://github.com/majn/telegram-purple.git
# mv telegram-purple purple-telegram-1.2.5
# tar -cvzf purple-telegram-1.2.5.tar.gz purple-telegram-1.2.5
Source0:	purple-telegram-%{version}.tar.gz
ExcludeArch:	ppc64
BuildRequires:	gettext
BuildRequires:	libgcrypt-devel >= 1.6
BuildRequires:	pkgconfig(zlib)
BuildRequires:	pkgconfig(purple)
BuildRequires:	pkgconfig(libwebp)
BuildRequires:	libappstream-glib

%description
Adds support for Telegram IM to purple-based clients such as Pidgin.

%prep
%autosetup

%build
%configure
make %{?_smp_mflags}

%install
%make_install
chmod 755 %{buildroot}/%{_libdir}/purple-2/telegram-purple.so
install -Dm 0644 -p telegram-purple.metainfo.xml \
	%{buildroot}%{_datadir}/appdata/telegram-purple.metainfo.xml
%find_lang telegram-purple

%check
appstream-util validate-relax --nonet %{buildroot}/%{_datadir}/appdata/telegram-purple.metainfo.xml

%files -f telegram-purple.lang
%license COPYING
%doc README* CHANGELOG*
%{_libdir}/purple-2/telegram-purple.so
%dir %{_sysconfdir}/telegram-purple/
%config %{_sysconfdir}/telegram-purple/*

#Icons
%dir %{_datadir}/pixmaps/pidgin/
%{_datadir}/pixmaps/pidgin/protocols/*/telegram.png

#AppStream metadata
%{_datadir}/appdata/telegram-purple.metainfo.xml

%changelog
* Thu Feb 04 2016 Fedora Release Engineering <releng@fedoraproject.org> - 1.2.5-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Mon Jan 25 2016 Jiri Eischmann <eischmann@redhat.com> - 1.2.5-1
- Update to 1.2.5
- Excluding ppc64 arch for now because it fails to build there

* Tue Jan 5 2016 Jiri Eischmann <eischmann@redhat.com> - 1.2.4-1
- Update to 1.2.4

* Tue Dec 29 2015 Kalev Lember <klember@redhat.com> - 1.2.2-5
- Rebuilt for libwebp soname bump

* Mon Dec 21 2015 Jiri Eischmann <eischmann@redhat.com> 1.2.2-4
- Specifying required version of libgcrypt-devel
- Changing summary

* Wed Dec 16 2015 Jiri Eischmann <eischmann@redhat.com> 1.2.2-3
- Another fix of ownership of protocol icon directories

* Tue Dec 15 2015 Jiri Eischmann <eischmann@redhat.com> 1.2.2-2
- Changed the way metainfo.xml file is installed, added validation for it
- Translation files now fully handled by macro
- BuildRequires now one per line
- Fixed ownership of directories of protocol icons

* Sat Dec 12 2015 Jiri Eischmann <eischmann@redhat.com> 1.2.2-1
- Update to 1.2.2
- Improved the spec (adding description, using pkgconfig for buildrequires, adding docs,...)
- Added translations
- Using libgcrypt instead of openssl

* Thu Sep 17 2015 Jiri Eischmann <eischmann@redhat.com> 1.2.1-1
- Initial build
